package main

//go:generate go run gen.go

import (
	"io"
	"jetson/darksky"
	"jetson/pir"
	"jetson/sesame"
	"jetson/transportation"
	"jetson/web"

	"fmt"
	"net/http"
	"runtime"
	"time"

	log "github.com/Sirupsen/logrus"
	"github.com/spf13/viper"
)

func main() {

	log.SetFormatter(&log.JSONFormatter{})

	log.Println("Starting...")
	log.Info(fmt.Sprintf("Built: %s", buildTime))
	log.Info(fmt.Sprintf("Version: %s", buildVersion))

	viper.SetConfigName("config")
	viper.AddConfigPath(".")
	viper.SetDefault("server.port", "8888")

	err := viper.ReadInConfig()
	if err != nil {
		log.Error(fmt.Errorf("Fatal error reading config file: %s", err))
	}

	darkskyConfig := darksky.NewConfig(
		viper.GetString("darksky.key"),
		viper.GetString("darksky.coordinates"),
		viper.GetString("darksky.query_string"),
	)

	sesameConfig := sesame.NewConfig(
		viper.GetString("sesame.lock_id"),
		viper.GetString("sesame.auth_token"),
	)

	// Initialize the PIR sensor
	if runtime.GOARCH == "arm" {
		log.Println("Initializing PIR")
		pir.Init()
	} else {
		// Set the screen to on, so functionality that depends on it will work (transportation)
		// This will cause a high number of API requests to be sent to the TrafikLab API.
		// Potentially request a higher level API key. See readme.md "TrafikLab API" section for more information.
		pir.ScreenStatus = true
	}

	// Initialize Darksky
	if viper.GetBool("services.weather.enabled") {
		log.Println("Initializing DarkSky")
		darksky.Init(darkskyConfig)
	}

	if viper.GetBool("services.sesame.enabled") {
		log.Println("Initializing Sesame")
		sesame.Init(sesameConfig)
	}

	// Initialize transportation
	if viper.GetBool("services.transportation.enabled") {
		log.Println("Initializing Transportation")
		transportation.Init()
	}

	// Watch ScreenStatus and fire requests as soon as the screen comes on
	log.Println("Setting up ScreenStatus watch loop")
	go func() {
		lastStatus := false
		for {
			if pir.ScreenStatus != lastStatus {
				if pir.ScreenStatus == true {
					log.Println("Screen turned on. Fetching data.")
					transportation.Fetch()
					darksky.Fetch(darkskyConfig)
				} else {
					log.Println("Screen turned off.")
				}

				lastStatus = !lastStatus
			}
			time.Sleep(time.Millisecond * 10)
		}
	}()

	log.Println("Registering routes")

	http.Handle("/departures", web.RecoverFailures(transportation.GetDepartures))
	http.Handle("/disruptions", web.RecoverFailures(transportation.GetDisruptions))
	http.Handle("/weather", web.RecoverFailures(darksky.GetWeatherForecast))

	http.Handle("/sesame/status", web.RecoverFailures(sesame.Status))

	http.Handle("/version", web.RecoverFailures(getVersion))

	fs := http.FileServer(http.Dir("static"))
	http.Handle("/", fs)

	log.Info("Running...")
	port := viper.GetString("server.port")
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", port), nil))
}

func getVersion(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("Access-Control-Allow-Origin", "*")

		w.WriteHeader(200)
		io.WriteString(w, fmt.Sprintf("{\"version\": \"%s\", \"built\": \"%s\"}", buildVersion, buildTime))
		return
	}
}
