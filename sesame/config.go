package sesame

const sesameBaseURL = "https://api.candyhouse.co/v1/"

// Config - Holds configuration for Darksky api calls
type Config struct {
	key       string
	authToken string
	baseURL   string
}

// NewConfig - Create a config object for Darksky
func NewConfig(key string, authToken string) Config {

	config := Config{
		baseURL: sesameBaseURL,
	}

	config.key = key
	config.authToken = authToken

	return config
}

func (c *Config) buildURL(action string) string {
	if action == "status" {
		return c.baseURL + "sesames"
	}

	return c.baseURL
}
