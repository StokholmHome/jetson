package sesame

// LockStatus - Represents the status of a lock
type LockStatus struct {
	Name       string `json:"nickname"`
	IsUnlocked bool   `json:"is_unlocked"`
	APIEnabled bool   `json:"api_enabled"`
	Battery    int    `json:"battery"`
}

// SetLock - Struct for exposing lock controls to clients
type SetLock struct {
	Status string `json:"status"`
}

// Control - Struct for creating JSON payload to control the lock
type Control struct {
	Type string `json:"type"`
}
