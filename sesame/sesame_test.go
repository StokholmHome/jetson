package sesame

import (
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestInit(t *testing.T) {
	config := NewConfig("a", "b")
	Init(config)

	if sharedConfig.key != "a" {
		t.Error("Failed to set key through Init")
	}

	if sharedConfig.authToken != "b" {
		t.Error("Failed to set authToken through Init")
	}
}

func TestUpdateLockStatus(t *testing.T) {
	config := NewConfig("a", "b")
	UpdateLockStatus(config)
}

func TestGetLockStatus(t *testing.T) {
	var apiStub = httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		io.WriteString(w, sesameSampleResponse)
	}))

	config := NewConfig("", "")
	config.baseURL = apiStub.URL + "/"

	status, err := getLockStatus(config)

	if err != nil {
		t.Error("Got error, expected nil")
	}

	if status.IsUnlocked != false {
		t.Error("Expected IsUnlocked to be false, it wasn't")
	}

	config.baseURL = ""
	status, err = getLockStatus(config)

	if err == nil {
		t.Error("Expected error, didn't get one")
	}

	apiStub = httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		io.WriteString(w, sesameBadSampleResponse)
	}))
	config.baseURL = apiStub.URL + "/"

	status, err = getLockStatus(config)
	if err == nil {
		t.Error("Didn't get an error.. Expected one.")
	}

}

func TestUpdateLock(t *testing.T) {
	var apiStub = httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusNoContent)
		io.WriteString(w, "")
	}))

	config := NewConfig("", "")
	config.baseURL = apiStub.URL + "/"

	sharedConfig = config

	err := updateLock("unlock")
	if err != nil {
		t.Error("Got error, shouldn't have.", err)
	}

	mu.Lock()
	sharedConfig.baseURL = ""
	mu.Unlock()

	err = updateLock("unlock")
	if err == nil {
		t.Error("Expected error, didn't get one")
	}
}

// func TestStatusOptions(t *testing.T) {
// 	var apiStub = httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
// 		w.Header().Set("Access-Control-Allow-Origin", "*")
// 		w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
// 		w.WriteHeader(http.StatusOK)
// 		io.WriteString(w, "")
// 	}))
// }

const sesameSampleResponse = `{"nickname": "Home","is_unlocked": false,"api_enabled": true,"battery": 83}`
const sesameBadSampleResponse = `"nickname": "Home","is_unlocked": false,"api_enabled": true,"battery": 83}`
