package sesame

import (
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	"sync"
	"time"
)

var lockJSON []byte
var sharedConfig Config
var mu sync.Mutex

// Init - Initialize lock handling
func Init(config Config) {
	sharedConfig = config
	go func() {
		for {
			UpdateLockStatus(config)
			time.Sleep(1 * time.Minute)
		}
	}()
}

// UpdateLockStatus - Fetches lock status from candyhouse
func UpdateLockStatus(config Config) {
	go func() {
		status, err := getLockStatus(config)
		if err == nil {
			mu.Lock()
			lockJSON, _ = json.MarshalIndent(status, "", "\t")
			mu.Unlock()
		}
	}()
}

// Status - Return/set lock status to/from the API
func Status(w http.ResponseWriter, r *http.Request) {
	if r.Method == "OPTIONS" {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
		w.WriteHeader(200)
		io.WriteString(w, "")
		return
	}

	if r.Method == "GET" {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
		w.Header().Set("Access-Control-Allow-Origin", "*")

		w.WriteHeader(200)

		mu.Lock()
		if len(lockJSON) == 0 {
			io.WriteString(w, "[]")
		} else {
			io.WriteString(w, string(lockJSON))
		}
		mu.Unlock()
		return
	}

	if r.Method == "POST" {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
		w.Header().Set("Access-Control-Allow-Origin", "*")

		payload := SetLock{}

		decoder := json.NewDecoder(r.Body)
		err := decoder.Decode(&payload)
		if err != nil {
			w.WriteHeader(400)
			io.WriteString(w, `{"error": "Bad json body supplied"}`)
			return
		}

		defer r.Body.Close()

		go func() {
			updateLock(payload.Status)
		}()

		w.WriteHeader(204)
		io.WriteString(w, "")

	}
}

func updateLock(action string) error {
	url := sharedConfig.buildURL("status")
	url = url + "/" + sharedConfig.key + "/control"

	control := Control{
		Type: action,
	}
	jsonString, _ := json.MarshalIndent(control, "", "\t")

	client := http.Client{
		Timeout: time.Duration(5 * time.Second),
	}
	request, _ := http.NewRequest("POST", url, bytes.NewBuffer(jsonString))

	request.Header.Add("X-Authorization", sharedConfig.authToken)
	request.Header.Add("Content-Type", "application/json")
	response, err := client.Do(request)
	if err != nil {
		return err
	}

	if response.StatusCode == http.StatusNoContent {
		// Check the lock status a little more often for a while (25 times with a 5 seconds sleep)
		go func() {
			for i := 0; i < 25; i++ {
				mu.Lock()
				UpdateLockStatus(sharedConfig)
				mu.Unlock()
				time.Sleep(5 * time.Second)
			}
		}()
	}

	return nil
}

func getLockStatus(config Config) (LockStatus, error) {

	lockStatus := LockStatus{}

	url := config.buildURL("status")
	url = url + "/" + config.key

	client := http.Client{
		Timeout: time.Duration(5 * time.Second),
	}
	request, _ := http.NewRequest("GET", url, nil)

	request.Header.Add("X-Authorization", config.authToken)
	response, err := client.Do(request)
	if err != nil {
		return lockStatus, err
	}

	jsonBody, _ := ioutil.ReadAll(response.Body)
	err = json.Unmarshal(jsonBody, &lockStatus)
	if err != nil {
		return lockStatus, err
	}

	return lockStatus, nil
}
