package pir

import (
	"testing"
	"time"
)

func TestScreenOn(t *testing.T) {
	ScreenStatus = false
	screenOn()

	if !ScreenStatus {
		t.Error("Screen is not on")
	}

	turnOffTime := screenOffAt.Sub(time.Now())
	dur, _ := time.ParseDuration("2m")

	if turnOffTime > dur {
		t.Errorf("Screen should turn off before %s, turns off at", screenOffAt)
	}
}

func TestScreenOff(t *testing.T) {
	ScreenStatus = true
	screenOff()

	if ScreenStatus {
		t.Error("The screen did not turn off")
	}
}
