package pir

// import (
// 	"io/ioutil"
// 	"os"
// 	"os/exec"
// 	"path/filepath"
// 	"strconv"
// 	"strings"
// 	"time"

// 	log "github.com/Sirupsen/logrus"
// 	rpio "github.com/stianeikeland/go-rpio"
// )

// var screenOffAt time.Time

// // ScreenStatus - Boolean depicting if the screen is on or not
// var ScreenStatus = false
// var screenPath = "/sys/class/backlight/rpi_backlight/"

// // Init - Run the PIR sensor functionality
// func Init() {
// 	go func() {

// 		defer func() {
// 			if r := recover(); r != nil {
// 				log.Printf("pir: Init recovering... %s", r)
// 				return
// 			}
// 		}()

// 		err := rpio.Open()
// 		if err != nil {
// 			log.Error(err)
// 		}

// 		defer rpio.Close()

// 		pin := rpio.Pin(5)
// 		pin.Input()

// 		for {
// 			res := pin.Read()
// 			log.Println("Pin read: ", res)
// 			if res == 1 && ScreenStatus != true {
// 				screenOn()
// 			}

// 			if screenOffAt.Sub(time.Now()) <= 0 {
// 				go func() {
// 					screenOff()
// 				}()
// 			}

// 			time.Sleep(500 * time.Millisecond)
// 		}
// 	}()
// }

// func screenOn() {
// 	log.Println("screenOn() called")
// 	if ScreenStatus == false {
// 		fadeScreenOn(350)

// 		ScreenStatus = true
// 	}

// 	screenOffAt = time.Now().Add(2 * time.Minute)
// }

// func screenOff() {
// 	log.Println("screenOff() called")
// 	// cmd := exec.Command("bash", "-c", "DISPLAY=:0 xset dpms force off")
// 	// err := cmd.Run()

// 	fadeScreenOff(350)

// 	ScreenStatus = false

// 	// if err != nil {
// 	// 	log.Error(err)
// 	// }
// }

// // Raspberry PI screen brightness functions
// func getByName(name string) int {
// 	log.Println("Getting brightness by name: ", name)
// 	path := filepath.Join(screenPath, name)
// 	data, err := ioutil.ReadFile(path)
// 	if err != nil {
// 		log.Error("Failed opening screen brightness file", err)
// 	}

// 	stringData := strings.TrimSuffix(string(data), "\n")
// 	result, err := strconv.Atoi(stringData)
// 	if err != nil {
// 		log.Error("Failed converting value to integer", err)
// 	}

// 	return result
// }

// func setByNameAndValue(name string, value int) (bool, error) {
// 	path := filepath.Join(screenPath, name)

// 	val := []byte(strconv.Itoa(value))
// 	err := ioutil.WriteFile(path, val, os.FileMode(os.O_WRONLY))
// 	if err != nil {
// 		log.Error("Failed writing value to brightness file", err)
// 		return false, err
// 	}

// 	return true, nil
// }

// func getMaxBrightness() int {
// 	return getByName("max_brightness")
// }

// func getBrightness() int {
// 	return getByName("actual_brightness")
// }

// func fadeScreenOn(duration int) {
// 	startBrightness := 0
// 	currentBrightness := startBrightness
// 	maxBrightness := getMaxBrightness()
// 	diff := int(maxBrightness) - int(startBrightness)

// 	cmd := exec.Command("bash", "-c", "DISPLAY=:0 xset dpms force on")
// 	err := cmd.Run()
// 	if err != nil {
// 		log.Error(err)
// 	}
// 	setByNameAndValue("brightness", startBrightness)

// 	for currentBrightness < maxBrightness {
// 		currentBrightness := currentBrightness + 1

// 		setByNameAndValue("brightness", currentBrightness)

// 		sleepTime := duration / diff
// 		time.Sleep(time.Duration(sleepTime) * time.Millisecond)
// 	}
// }

// func fadeScreenOff(duration int) {
// 	startBrightness := getBrightness()
// 	currentBrightness := startBrightness

// 	for currentBrightness > 0 {

// 		currentBrightness = currentBrightness - 1
// 		setByNameAndValue("brightness", currentBrightness)

// 		sleepTime := duration / startBrightness
// 		time.Sleep(time.Duration(sleepTime) * time.Millisecond)
// 	}

// 	cmd := exec.Command("bash", "-c", "DISPLAY=:0 xset dpms force off")
// 	err := cmd.Run()
// 	if err != nil {
// 		log.Error(err)
// 	}
// }
