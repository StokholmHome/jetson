package pir

import (
	"os/exec"
	"time"

	log "github.com/Sirupsen/logrus"
	rpio "github.com/stianeikeland/go-rpio"
)

var screenOffAt time.Time

// ScreenStatus - Boolean depicting if the screen is on or not
var ScreenStatus = false

// Init - Run the PIR sensor functionality
func Init() {
	go func() {

		defer func() {
			if r := recover(); r != nil {
				log.Printf("pir: Init recovering... %s", r)
				return
			}
		}()

		err := rpio.Open()
		if err != nil {
			log.Error(err)
		}

		defer rpio.Close()

		pin := rpio.Pin(5)
		pin.Input()

		for {
			res := pin.Read()
			if res == 1 {
				screenOn()
			}

			if screenOffAt.Sub(time.Now()) <= 0 {
				go func() {
					screenOff()
				}()
			}

			time.Sleep(time.Second / 2) // Sleep for 500ms
		}
	}()
}

func screenOn() {
	if ScreenStatus == false {
		cmd := exec.Command("bash", "-c", "DISPLAY=:0 xset dpms force on")
		err := cmd.Run()
		if err != nil {
			log.Error(err)
		}

		ScreenStatus = true
	}

	screenOffAt = time.Now().Add(2 * time.Minute)
}

func screenOff() {
	cmd := exec.Command("bash", "-c", "DISPLAY=:0 xset dpms force off")
	err := cmd.Run()

	ScreenStatus = false

	if err != nil {
		log.Error(err)
	}
}
