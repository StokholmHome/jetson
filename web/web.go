package web

import (
	"fmt"
	"net/http"

	log "github.com/Sirupsen/logrus"
)

// RecoverFailures - Middleware for not crashing on errors in HTTP handling
func RecoverFailures(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if r := recover(); r != nil {
				errorString := fmt.Sprintf("%s", r)
				log.Printf("Recovering... %s", r)

				internalError(w, errorString)
				return
			}
		}()
		next(w, r)
	}
}

func internalError(w http.ResponseWriter, message string) {
	errorString := fmt.Sprintf("{\"error\": \"%s\"}", message)
	http.Error(w, errorString, http.StatusInternalServerError)
	return
}
