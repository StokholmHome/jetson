package transportation

import (
	"jetson/pir"

	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"sort"
	"strconv"
	"strings"
	"time"

	log "github.com/Sirupsen/logrus"
	"github.com/go-restit/lzjson"
	"github.com/spf13/viper"
)

var departureJSON []byte
var disruptionJSON []byte

// Init - Initialize transportation
func Init() {

	go func() {
		for {
			if pir.ScreenStatus == true {
				log.Println("Departure loop")
				departures := fetchDepartures()
				if len(departures) > 0 {
					var err error
					departureJSON, err = json.MarshalIndent(departures, "", "\t")
					if err != nil {
						fmt.Println(err)
						return
					}
				}
			}

			time.Sleep(30 * time.Second)
		}
	}()

	go func() {
		for {
			if pir.ScreenStatus == true {
				log.Info("Disruptions loop")
				disruptions := fetchDisruptions()
				if len(disruptions) > 0 {
					var err error
					disruptionJSON, err = json.MarshalIndent(disruptions, "", "\t")
					if err != nil {
						fmt.Println(err)
						return
					}
				}
			}

			time.Sleep(5 * time.Minute)
		}
	}()

}

// Fetch - Fetch transportation data outside timed loops
func Fetch() {
	go func() {
		departures := fetchDepartures()
		if len(departures) > 0 {
			var err error
			departureJSON, err = json.MarshalIndent(departures, "", "\t")
			if err != nil {
				fmt.Println(err)
				return
			}
		}
	}()

	go func() {
		disruptions := fetchDisruptions()
		if len(disruptions) > 0 {
			var err error
			disruptionJSON, err = json.MarshalIndent(disruptions, "", "\t")
			if err != nil {
				fmt.Println(err)
				return
			}
		}
	}()
}

func fetchDepartures() []SimpleDeparture {

	var departureSites []DepartureSite
	viper.UnmarshalKey("departures.sites", &departureSites)

	var departureMessage []SimpleDeparture

	for _, site := range departureSites {
		var siteID = strconv.Itoa(site.SiteID)
		var transportMode = site.TransportMode

		log.Println("Sending departure request")
		response, err := http.Get("http://api.sl.se/api2/realtimedeparturesV4.json?key=" + viper.GetString("departures.api.key") + "&siteid=" + siteID + "&timewindow=" + viper.GetString("departures.timewindow"))
		if err != nil {
			log.Fatal(err)
			return departureMessage
		}

		var departures []Departure
		json := lzjson.Decode(response.Body).Get("ResponseData").Get(transportMode)
		json.Unmarshal(&departures)

		for _, departure := range departures {

			sort.Ints(site.Directions)
			directionCount := sort.Search(len(site.Directions), func(directionCount int) bool {
				return site.Directions[directionCount] >= departure.JourneyDirection
			})

			if directionCount < len(site.Directions) && site.Directions[directionCount] == departure.JourneyDirection {
				formattedDeparture := SimpleDeparture{
					TransportMode:    departure.TransportMode,
					TimeOfDeparture:  departure.DisplayTime,
					JourneyDirection: departure.JourneyDirection,
				}

				departureMessage = append(departureMessage, formattedDeparture)
			}
		}
	}

	return departureMessage
}

func fetchDisruptions() []SimpleDisruption {

	var disruptionMessage []SimpleDisruption
	for _, transportMode := range viper.GetStringSlice("disruptions.transport_modes") {
		log.Printf("Sending disruption request for transportmode: %s", transportMode)
		response, err := http.Get("http://api.sl.se/api2/deviations.json?key=" + viper.GetString("disruptions.api.key") + "&transportMode=" + transportMode + "&lineNumber=" + viper.GetString("disruptions.lines"))
		if err != nil {
			log.Fatal(err)
		}

		var disruptions []Disruption
		json := lzjson.Decode(response.Body).Get("ResponseData")
		json.Unmarshal(&disruptions)

		ignoreWords := viper.GetStringSlice("disruptions.ignore_words")

		for _, disruption := range disruptions {
			includeDisruption := true
			for _, word := range ignoreWords {
				if strings.Contains(disruption.Header, word) || strings.Contains(disruption.Details, word) {
					includeDisruption = false
				}
			}

			if includeDisruption {
				formattedDisruption := SimpleDisruption{
					Header:        disruption.Header,
					Details:       disruption.Details,
					Scope:         disruption.Scope,
					TransportMode: transportMode,
				}

				disruptionMessage = append(disruptionMessage, formattedDisruption)
			}
		}
	}

	return disruptionMessage
}

// GetDepartures - Return departures to the API
func GetDepartures(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("Access-Control-Allow-Origin", "*")

		w.WriteHeader(200)
		if len(departureJSON) == 0 {
			io.WriteString(w, "[]")
		} else {
			io.WriteString(w, string(departureJSON))
		}
		return
	}
}

// GetDisruptions - Return disruptions to the API
func GetDisruptions(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("Access-Control-Allow-Origin", "*")

		w.WriteHeader(200)

		if len(disruptionJSON) == 0 {
			io.WriteString(w, "[]")
		} else {
			io.WriteString(w, string(disruptionJSON))
		}
		return
	}
}
