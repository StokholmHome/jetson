package transportation

import "time"

// DepartureSite - Config structure for departure sites
type DepartureSite struct {
	Directions    []int
	SiteID        int    `mapstructure:"site_id"`
	TransportMode string `mapstructure:"transport_mode"`
}

// Departure - Structure of departures from the SL API
type Departure struct {
	GroupOfLine          string
	DisplayTime          string
	TransportMode        string
	LineNumber           string
	Destination          string
	JourneyDirection     int
	StopAreaName         string
	StopAreaNumber       int
	StopPointNumber      int
	StopPointDesignation string
	TimeTabledDateTime   string
	ExpectedDateTime     string
	JourneyNumber        int
	Deviations           []string
}

// SimpleDeparture - The structure of the object to be used
type SimpleDeparture struct {
	TransportMode    string `json:"transport_mode"`
	TimeOfDeparture  string `json:"time_of_departure"`
	JourneyDirection int    `json:"journey_direction"`
}

// Disruption - SL disruption response
type Disruption struct {
	Created                 time.Time `json:"Created"`
	MainNews                bool      `json:"MainNews"`
	SortOrder               int       `json:"SortOrder"`
	Header                  string    `json:"Header"`
	Details                 string    `json:"Details"`
	Scope                   string    `json:"Scope"`
	DevCaseGid              int64     `json:"DevCaseGid"`
	DevMessageVersionNumber int       `json:"DevMessageVersionNumber"`
	ScopeElements           string    `json:"ScopeElements"`
	FromDateTime            string    `json:"FromDateTime"`
	UpToDateTime            string    `json:"UpToDateTime"`
	Updated                 time.Time `json:"Updated"`
}

// SimpleDisruption - Extract of disruption data needed to represent issues
type SimpleDisruption struct {
	Header        string `json:"header"`
	Details       string `json:"details"`
	Scope         string `json:"scope"`
	TransportMode string `json:"transport_mode"`
}
