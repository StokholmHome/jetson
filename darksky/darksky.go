package darksky

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	"sync"
	"time"

	log "github.com/Sirupsen/logrus"
	"github.com/spf13/viper"
)

var forecastJSON []byte
var mu sync.Mutex

// Init - Initialize weather forecast
func Init(config Config) {
	go func() {
		for {
			Fetch(config)
			time.Sleep(10 * time.Minute)
		}
	}()
}

// Fetch - Fetch weather forecast data outside timed loops
func Fetch(config Config) {
	go func() {
		forecast, err := fetchWeatherForecast(config)
		if err == nil {
			mu.Lock()
			forecastJSON, _ = json.MarshalIndent(forecast, "", "\t")
			mu.Unlock()
		}
	}()
}

// GetWeatherForecast - Return weather forecasts to the API
func GetWeatherForecast(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("Access-Control-Allow-Origin", "*")

		w.WriteHeader(200)

		if len(forecastJSON) == 0 {
			io.WriteString(w, "[]")
		} else {
			mu.Lock()
			io.WriteString(w, string(forecastJSON))
			mu.Unlock()
		}
		return
	}
}

func fetchWeatherForecast(config Config) ([]SimpleForecast, error) {

	forecast := WeatherForecast{}

	log.Println("Sending weather request")
	response, err := http.Get(config.buildURL())
	if err != nil {
		return nil, err
	}

	jsonBody, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err

	}

	unmarshalErr := json.Unmarshal(jsonBody, &forecast)
	if unmarshalErr != nil {
		return nil, unmarshalErr
	}

	forecastData := forecast
	collection := []SimpleForecast{}

	tempForecast := SimpleForecast{
		MinTemp:      -999,
		MaxTemp:      -999,
		StartTimeInt: -1,
		EndTimeInt:   -1,
	}

	tz, _ := time.LoadLocation(viper.GetString("tz_location"))

	iconMap := map[string]int{
		"clear-night":         0,
		"partly-cloudy-night": 1,
		"clear-day":           2,
		"partly-cloudy-day":   3,
		"cloudy":              4,
		"fog":                 5,
		"wind":                6,
		"rain":                7,
		"snow":                8,
		"sleet":               9,
	}

	for i, forecast := range forecastData.Hourly.Data {
		if i > 16 {
			break
		}

		loopCount := i + 1

		if forecast.Temperature < tempForecast.MinTemp || tempForecast.MinTemp == -999 {
			tempForecast.MinTemp = forecast.Temperature
		}

		if forecast.Temperature > tempForecast.MaxTemp || tempForecast.MaxTemp == -999 {
			tempForecast.MaxTemp = forecast.Temperature
		}

		if forecast.Time < tempForecast.StartTimeInt || tempForecast.StartTimeInt == -1 {
			tempForecast.StartTimeInt = forecast.Time
		}

		if forecast.Time > tempForecast.EndTimeInt || tempForecast.EndTimeInt == -1 {
			tempForecast.EndTimeInt = forecast.Time
		}

		if iconMap[forecast.Icon] > iconMap[tempForecast.Icon] || tempForecast.Icon == "" {
			tempForecast.Icon = forecast.Icon
		}

		if loopCount%4 == 0 {

			startTime := time.Unix(tempForecast.StartTimeInt, 0).In(tz).Format("15")
			tempForecast.StartHour = startTime

			endTime := time.Unix(tempForecast.EndTimeInt, 0).In(tz).Format("15")
			tempForecast.EndHour = endTime

			collection = append(collection, tempForecast)

			tempForecast = SimpleForecast{
				MinTemp:      -999,
				MaxTemp:      -999,
				StartTimeInt: -1,
				EndTimeInt:   -1,
			}
		}
	}

	return collection, nil
}
