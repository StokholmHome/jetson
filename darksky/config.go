package darksky

const darkskyBaseURL = "https://api.darksky.net/forecast/"

// Config - Holds configuration for Darksky api calls
type Config struct {
	key         string
	coordinates string
	queryString string
	baseURL     string
}

// NewConfig - Create a config object for Darksky
func NewConfig(key string, coordinates string, queryString string) Config {

	config := Config{
		baseURL: darkskyBaseURL,
	}

	config.key = key
	config.coordinates = coordinates
	config.queryString = queryString

	return config
}

func (c *Config) buildURL() string {
	return c.baseURL + c.key + "/" + c.coordinates + c.queryString
}
