package darksky

// WeatherForecast - Darksky forecast root object
type WeatherForecast struct {
	Latitude  float64
	Longitude float64
	Timezone  string
	TZOffset  int `json:"offset"`
	Hourly    HourlyForecastList
}

// HourlyForecastList - Darksky forecast list structure
type HourlyForecastList struct {
	Summary string
	Icon    string
	Data    []HourlyForecast
}

// HourlyForecast - Darksky forecast structure
type HourlyForecast struct {
	Time                int64   `json:"time"`
	Summary             string  `json:"summary"`
	Icon                string  `json:"icon"`
	PrecipIntensity     float64 `json:"precipIntensity"`
	PrecipProbability   float64 `json:"precipProbability"`
	PrecipType          string  `json:"precipType"`
	Temperature         float64 `json:"temperature"`
	ApparentTemperature float64 `json:"apparentTemperature"`
	DewPoint            float64 `json:"dewPoint"`
	Humidity            float64 `json:"humidity"`
	Pressure            float64 `json:"pressure"`
	WindSpeed           float64 `json:"windSpeed"`
	WindGust            float64 `json:"windGust"`
	WindBearing         int     `json:"windBearing"`
	CloudCover          float64 `json:"cloudCover"`
	UvIndex             int     `json:"uvIndex"`
	Visibility          float64 `json:"visibility"`
	Ozone               float64 `json:"ozone"`
}

// SimpleForecast - Darksky data needed to display forecast
type SimpleForecast struct {
	MinTemp      float64 `json:"min_temp"`
	MaxTemp      float64 `json:"max_temp"`
	Icon         string  `json:"icon"`
	StartTimeInt int64   `json:"-"`
	EndTimeInt   int64   `json:"-"`
	StartHour    string  `json:"start_hour"`
	EndHour      string  `json:"end_hour"`
}
