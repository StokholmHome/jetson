package darksky

import "testing"

func TestNewConfig(t *testing.T) {
	config := NewConfig("key", "coordinates", "queryString")

	if config.baseURL != darkskyBaseURL {
		t.Error("Base URL is not as expected")
	}

	if config.key != "key" {
		t.Error("Key is not correctly set")
	}

	if config.coordinates != "coordinates" {
		t.Error("Coordinates is not correctly set")
	}

	if config.queryString != "queryString" {
		t.Error("queryString is not correctly set")
	}
}

func TestBuildURL(t *testing.T) {
	config := NewConfig("key", "coordinates", "queryString")

	url := config.buildURL()

	if url != darkskyBaseURL+config.key+"/"+config.coordinates+config.queryString {
		t.Error("URL is not built correctly")
	}
}
