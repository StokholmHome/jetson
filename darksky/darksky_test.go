package darksky

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
)

func TestFetchWeatherForecast(t *testing.T) {

	// Set up mock http server
	var apiStub = httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		io.WriteString(w, darkskySampleResponse)
	}))

	config := NewConfig("", "", "")
	config.baseURL = apiStub.URL

	forecast, err := fetchWeatherForecast(config)

	if err != nil {
		t.Error("Got error, expected nil")
	}

	if len(forecast) != 4 {
		t.Error("Incorrect number of forecasts returned")
	}

	if forecast[0].Icon != "clear-night" {
		t.Error("Incorrect icon detected")
	}

	if forecast[0].StartHour != "21" {
		t.Error("Start hour is not 21")
	}

	if forecast[0].EndHour != "00" {
		t.Error("End hour is not 00")
	}
}

func TestFetchWeatherForecastHTTPError(t *testing.T) {

	config := NewConfig("", "", "")
	config.baseURL = "badurl"

	_, err := fetchWeatherForecast(config)

	if err == nil {
		t.Error("Expected an error, got nil")
	}
}

func TestFetchWeatherForecastBadJSON(t *testing.T) {

	// Set up mock http server
	var apiStub = httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		io.WriteString(w, "badjson")
	}))

	config := NewConfig("", "", "")
	config.baseURL = apiStub.URL

	_, err := fetchWeatherForecast(config)

	if err == nil {
		t.Error("Expected an error, got nil")
	}

	if err.Error() != "invalid character 'b' looking for beginning of value" {
		t.Error("Got unexpected error string:", err.Error())
	}
}

func TestFetch(t *testing.T) {
	// Set up mock http server
	var apiStub = httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		io.WriteString(w, darkskySampleResponse)
	}))

	config := NewConfig("", "", "")
	config.baseURL = apiStub.URL

	if len(forecastJSON) > 0 {
		t.Error("forecastJSON has value too early")
	}

	Fetch(config)

	time.Sleep(time.Millisecond * 50)

	mu.Lock()
	if len(forecastJSON) == 0 {
		t.Error("forecastJSON didn't get a value")
	}
	mu.Unlock()
}

type TestGetWeatherForecast struct{}

func (h *TestGetWeatherForecast) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	GetWeatherForecast(w, r)
}

func TestGetWeatherForecastResponse(t *testing.T) {
	h := &TestGetWeatherForecast{}
	server := httptest.NewServer(h)
	defer server.Close()

	response, err := http.Get(server.URL)
	if err != nil {
		t.Error("http.Get failed")
	}

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		t.Error("Reading response body faield")
	}

	if string(body) == "[]" {
		t.Errorf("Body was %s expected an array with length 4", body)
	}

	forecast := []SimpleForecast{}
	json.Unmarshal(body, &forecast)

	if len(forecast) != 4 {
		t.Errorf("JSON array length was %d, expected 4", len(forecast))
	}

	server.Close()
	return
}

func TestGetWeatherForecastEmptyResponse(t *testing.T) {
	h := &TestGetWeatherForecast{}
	server := httptest.NewServer(h)
	defer server.Close()

	forecastJSON = []byte{}
	response, err := http.Get(server.URL)
	if err != nil {
		t.Error("http.Get failed")
	}

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		t.Error("Reading response body faield")
	}

	if string(body) != "[]" {
		t.Errorf("Body was %s expected []", body)
	}

	return
}

const darkskySampleResponse = `{"latitude":59.290423,"longitude":17.997173,"timezone":"Europe/Stockholm","hourly":{"summary":"Mostly cloudy starting later tonight.","icon":"partly-cloudy-day","data":[{"time":1521579600,"summary":"Clear","icon":"clear-night","precipIntensity":0.0025,"precipProbability":0.01,"precipAccumulation":0,"precipType":"snow","temperature":-7.46,"apparentTemperature":-7.46,"dewPoint":-12.22,"humidity":0.69,"pressure":1020.72,"windSpeed":0.31,"windGust":0.79,"windBearing":195,"cloudCover":0,"uvIndex":0,"visibility":10.01,"ozone":458.17},{"time":1521583200,"summary":"Clear","icon":"clear-night","precipIntensity":0.0152,"precipProbability":0.02,"precipAccumulation":0.025,"precipType":"snow","temperature":-7.36,"apparentTemperature":-7.36,"dewPoint":-12.14,"humidity":0.69,"pressure":1020.76,"windSpeed":0.87,"windGust":1.1,"windBearing":110,"cloudCover":0.03,"uvIndex":0,"visibility":10.01,"ozone":457.45},{"time":1521586800,"summary":"Clear","icon":"clear-night","precipIntensity":0.0508,"precipProbability":0.03,"precipAccumulation":0.081,"precipType":"snow","temperature":-6.76,"apparentTemperature":-10.48,"dewPoint":-11.62,"humidity":0.68,"pressure":1020.71,"windSpeed":2.1,"windGust":2.1,"windBearing":147,"cloudCover":0.07,"uvIndex":0,"visibility":10.01,"ozone":455.92},{"time":1521590400,"summary":"Clear","icon":"clear-night","precipIntensity":0.0762,"precipProbability":0.04,"precipAccumulation":0.114,"precipType":"snow","temperature":-6.09,"apparentTemperature":-10.28,"dewPoint":-10.92,"humidity":0.69,"pressure":1020.33,"windSpeed":2.53,"windGust":2.75,"windBearing":197,"cloudCover":0.18,"uvIndex":0,"visibility":10.01,"ozone":454.26},{"time":1521594000,"summary":"Partly Cloudy","icon":"partly-cloudy-night","precipIntensity":0.0533,"precipProbability":0.03,"precipAccumulation":0.074,"precipType":"snow","temperature":-5.62,"apparentTemperature":-10.09,"dewPoint":-10.01,"humidity":0.71,"pressure":1019.48,"windSpeed":2.86,"windGust":4.31,"windBearing":214,"cloudCover":0.39,"uvIndex":0,"visibility":10.01,"ozone":452.49},{"time":1521597600,"summary":"Mostly Cloudy","icon":"partly-cloudy-night","precipIntensity":0.0178,"precipProbability":0.02,"precipAccumulation":0.025,"precipType":"snow","temperature":-5.06,"apparentTemperature":-9.88,"dewPoint":-8.89,"humidity":0.74,"pressure":1018.34,"windSpeed":3.3,"windGust":6.22,"windBearing":210,"cloudCover":0.66,"uvIndex":0,"ozone":450.47},{"time":1521601200,"summary":"Mostly Cloudy","icon":"partly-cloudy-night","precipIntensity":0.0051,"precipProbability":0.02,"precipAccumulation":0,"precipType":"snow","temperature":-4.63,"apparentTemperature":-9.65,"dewPoint":-7.93,"humidity":0.78,"pressure":1017.29,"windSpeed":3.61,"windGust":7.96,"windBearing":208,"cloudCover":0.86,"uvIndex":0,"ozone":448.85},{"time":1521604800,"summary":"Mostly Cloudy","icon":"partly-cloudy-night","precipIntensity":0.0025,"precipProbability":0.02,"precipAccumulation":0,"precipType":"snow","temperature":-4.48,"apparentTemperature":-9.54,"dewPoint":-7.23,"humidity":0.81,"pressure":1016.58,"windSpeed":3.71,"windGust":9.39,"windBearing":217,"cloudCover":0.93,"uvIndex":0,"ozone":447.81},{"time":1521608400,"summary":"Mostly Cloudy","icon":"partly-cloudy-day","precipIntensity":0.0025,"precipProbability":0.02,"precipAccumulation":0,"precipType":"snow","temperature":-4.31,"apparentTemperature":-9.31,"dewPoint":-6.69,"humidity":0.83,"pressure":1015.99,"windSpeed":3.67,"windGust":10.64,"windBearing":229,"cloudCover":0.92,"uvIndex":0,"ozone":447.05},{"time":1521612000,"summary":"Mostly Cloudy","icon":"partly-cloudy-day","precipIntensity":0.0025,"precipProbability":0.02,"precipAccumulation":0,"precipType":"snow","temperature":-3.82,"apparentTemperature":-8.69,"dewPoint":-6.08,"humidity":0.84,"pressure":1015.45,"windSpeed":3.66,"windGust":11.54,"windBearing":239,"cloudCover":0.89,"uvIndex":0,"ozone":445.87},{"time":1521615600,"summary":"Mostly Cloudy","icon":"partly-cloudy-day","precipIntensity":0,"precipProbability":0,"temperature":-2.84,"apparentTemperature":-7.6,"dewPoint":-5.32,"humidity":0.83,"pressure":1015.03,"windSpeed":3.77,"windGust":12.07,"windBearing":250,"cloudCover":0.79,"uvIndex":0,"ozone":443.01},{"time":1521619200,"summary":"Mostly Cloudy","icon":"partly-cloudy-day","precipIntensity":0,"precipProbability":0,"temperature":-1.61,"apparentTemperature":-6.19,"dewPoint":-4.48,"humidity":0.81,"pressure":1014.67,"windSpeed":3.91,"windGust":12.25,"windBearing":255,"cloudCover":0.65,"uvIndex":1,"ozone":439.57},{"time":1521622800,"summary":"Partly Cloudy","icon":"partly-cloudy-day","precipIntensity":0,"precipProbability":0,"temperature":-0.55,"apparentTemperature":-4.89,"dewPoint":-3.57,"humidity":0.8,"pressure":1014.08,"windSpeed":3.91,"windGust":11.9,"windBearing":250,"cloudCover":0.59,"uvIndex":1,"ozone":438.48},{"time":1521626400,"summary":"Mostly Cloudy","icon":"partly-cloudy-day","precipIntensity":0,"precipProbability":0,"temperature":0.41,"apparentTemperature":-3.56,"dewPoint":-2.47,"humidity":0.81,"pressure":1013.09,"windSpeed":3.67,"windGust":10.64,"windBearing":207,"cloudCover":0.66,"uvIndex":1,"ozone":442.12},{"time":1521630000,"summary":"Mostly Cloudy","icon":"partly-cloudy-day","precipIntensity":0,"precipProbability":0,"temperature":1.19,"apparentTemperature":-2.32,"dewPoint":-1.33,"humidity":0.83,"pressure":1011.84,"windSpeed":3.32,"windGust":8.85,"windBearing":311,"cloudCover":0.81,"uvIndex":1,"ozone":447.98},{"time":1521633600,"summary":"Mostly Cloudy","icon":"partly-cloudy-day","precipIntensity":0,"precipProbability":0,"temperature":2.12,"apparentTemperature":-0.97,"dewPoint":-0.49,"humidity":0.83,"pressure":1010.79,"windSpeed":3.02,"windGust":7.59,"windBearing":267,"cloudCover":0.92,"uvIndex":1,"ozone":451.92},{"time":1521637200,"summary":"Overcast","icon":"cloudy","precipIntensity":0,"precipProbability":0,"temperature":2.94,"apparentTemperature":0.13,"dewPoint":-0.18,"humidity":0.8,"pressure":1010.03,"windSpeed":2.88,"windGust":7.31,"windBearing":265,"cloudCover":0.96,"uvIndex":1,"ozone":451.56},{"time":1521640800,"summary":"Overcast","icon":"cloudy","precipIntensity":0,"precipProbability":0,"temperature":3.75,"apparentTemperature":1.14,"dewPoint":-0.21,"humidity":0.75,"pressure":1009.44,"windSpeed":2.82,"windGust":7.56,"windBearing":264,"cloudCover":0.96,"uvIndex":0,"ozone":449.17},{"time":1521644400,"summary":"Mostly Cloudy","icon":"partly-cloudy-day","precipIntensity":0,"precipProbability":0,"temperature":3.99,"apparentTemperature":1.44,"dewPoint":-0.43,"humidity":0.73,"pressure":1009.01,"windSpeed":2.82,"windGust":7.95,"windBearing":268,"cloudCover":0.9,"uvIndex":0,"ozone":447.91},{"time":1521648000,"summary":"Mostly Cloudy","icon":"partly-cloudy-day","precipIntensity":0,"precipProbability":0,"temperature":3.43,"apparentTemperature":0.7,"dewPoint":-0.89,"humidity":0.73,"pressure":1008.81,"windSpeed":2.91,"windGust":8.49,"windBearing":265,"cloudCover":0.72,"uvIndex":0,"ozone":449.61},{"time":1521651600,"summary":"Partly Cloudy","icon":"partly-cloudy-night","precipIntensity":0,"precipProbability":0,"temperature":2.14,"apparentTemperature":-0.98,"dewPoint":-1.57,"humidity":0.76,"pressure":1008.74,"windSpeed":3.07,"windGust":9.19,"windBearing":263,"cloudCover":0.47,"uvIndex":0,"ozone":452.44},{"time":1521655200,"summary":"Partly Cloudy","icon":"partly-cloudy-night","precipIntensity":0,"precipProbability":0,"temperature":1.15,"apparentTemperature":-2.28,"dewPoint":-2.13,"humidity":0.79,"pressure":1008.7,"windSpeed":3.19,"windGust":9.58,"windBearing":262,"cloudCover":0.3,"uvIndex":0,"ozone":454},{"time":1521658800,"summary":"Partly Cloudy","icon":"partly-cloudy-night","precipIntensity":0,"precipProbability":0,"temperature":0.42,"apparentTemperature":-3.23,"dewPoint":-2.42,"humidity":0.81,"pressure":1008.59,"windSpeed":3.28,"windGust":9.47,"windBearing":264,"cloudCover":0.25,"uvIndex":0,"ozone":453.55},{"time":1521662400,"summary":"Partly Cloudy","icon":"partly-cloudy-night","precipIntensity":0,"precipProbability":0,"temperature":-0.11,"apparentTemperature":-3.91,"dewPoint":-2.54,"humidity":0.84,"pressure":1008.48,"windSpeed":3.33,"windGust":9.07,"windBearing":268,"cloudCover":0.27,"uvIndex":0,"ozone":452.01},{"time":1521666000,"summary":"Partly Cloudy","icon":"partly-cloudy-night","precipIntensity":0.0025,"precipProbability":0.04,"precipAccumulation":0,"precipType":"snow","temperature":-0.65,"apparentTemperature":-4.51,"dewPoint":-2.78,"humidity":0.85,"pressure":1008.46,"windSpeed":3.26,"windGust":8.53,"windBearing":271,"cloudCover":0.33,"uvIndex":0,"ozone":449.06},{"time":1521669600,"summary":"Partly Cloudy","icon":"partly-cloudy-night","precipIntensity":0.0025,"precipProbability":0.04,"precipAccumulation":0,"precipType":"snow","temperature":-1.27,"apparentTemperature":-4.99,"dewPoint":-3.18,"humidity":0.87,"pressure":1008.6,"windSpeed":2.96,"windGust":7.85,"windBearing":263,"cloudCover":0.43,"uvIndex":0,"ozone":444.01},{"time":1521673200,"summary":"Partly Cloudy","icon":"partly-cloudy-night","precipIntensity":0.0025,"precipProbability":0.03,"precipAccumulation":0,"precipType":"snow","temperature":-1.91,"apparentTemperature":-5.36,"dewPoint":-3.66,"humidity":0.88,"pressure":1008.8,"windSpeed":2.57,"windGust":7.04,"windBearing":260,"cloudCover":0.57,"uvIndex":0,"ozone":437.53},{"time":1521676800,"summary":"Mostly Cloudy","icon":"partly-cloudy-night","precipIntensity":0,"precipProbability":0,"temperature":-2.43,"apparentTemperature":-5.71,"dewPoint":-4.01,"humidity":0.89,"pressure":1008.82,"windSpeed":2.34,"windGust":6.31,"windBearing":263,"cloudCover":0.68,"uvIndex":0,"ozone":431.93},{"time":1521680400,"summary":"Mostly Cloudy","icon":"partly-cloudy-night","precipIntensity":0,"precipProbability":0,"temperature":-2.78,"apparentTemperature":-6.19,"dewPoint":-4.16,"humidity":0.9,"pressure":1008.49,"windSpeed":2.39,"windGust":5.72,"windBearing":321,"cloudCover":0.76,"uvIndex":0,"ozone":427.72},{"time":1521684000,"summary":"Mostly Cloudy","icon":"partly-cloudy-night","precipIntensity":0,"precipProbability":0,"temperature":-2.98,"apparentTemperature":-6.67,"dewPoint":-4.2,"humidity":0.91,"pressure":1008,"windSpeed":2.6,"windGust":5.2,"windBearing":216,"cloudCover":0.81,"uvIndex":0,"ozone":424.31},{"time":1521687600,"summary":"Mostly Cloudy","icon":"partly-cloudy-night","precipIntensity":0,"precipProbability":0,"temperature":-3.08,"apparentTemperature":-7.01,"dewPoint":-4.21,"humidity":0.92,"pressure":1007.48,"windSpeed":2.8,"windGust":4.85,"windBearing":257,"cloudCover":0.84,"uvIndex":0,"ozone":422.43},{"time":1521691200,"summary":"Mostly Cloudy","icon":"partly-cloudy-night","precipIntensity":0,"precipProbability":0,"temperature":-3.34,"apparentTemperature":-7.39,"dewPoint":-4.18,"humidity":0.94,"pressure":1007.01,"windSpeed":2.88,"windGust":4.77,"windBearing":229,"cloudCover":0.87,"uvIndex":0,"ozone":422.85},{"time":1521694800,"summary":"Mostly Cloudy","icon":"partly-cloudy-day","precipIntensity":0,"precipProbability":0,"temperature":-3.46,"apparentTemperature":-7.36,"dewPoint":-4.08,"humidity":0.95,"pressure":1006.53,"windSpeed":2.71,"windGust":4.85,"windBearing":323,"cloudCover":0.88,"uvIndex":0,"ozone":424.88},{"time":1521698400,"summary":"Mostly Cloudy","icon":"partly-cloudy-day","precipIntensity":0,"precipProbability":0,"temperature":-3.05,"apparentTemperature":-7.18,"dewPoint":-3.72,"humidity":0.95,"pressure":1006.19,"windSpeed":3.02,"windGust":4.72,"windBearing":263,"cloudCover":0.81,"uvIndex":0,"ozone":426.55},{"time":1521702000,"summary":"Partly Cloudy","icon":"partly-cloudy-day","precipIntensity":0,"precipProbability":0,"temperature":-1.69,"apparentTemperature":-5.33,"dewPoint":-2.94,"humidity":0.91,"pressure":1006.07,"windSpeed":2.79,"windGust":3.84,"windBearing":210,"cloudCover":0.55,"uvIndex":0,"ozone":427.38},{"time":1521705600,"summary":"Clear","icon":"clear-day","precipIntensity":0,"precipProbability":0,"temperature":0.08,"apparentTemperature":-2.92,"dewPoint":-1.91,"humidity":0.87,"pressure":1006.09,"windSpeed":2.49,"windGust":2.75,"windBearing":322,"cloudCover":0.2,"uvIndex":1,"ozone":428.03},{"time":1521709200,"summary":"Clear","icon":"clear-day","precipIntensity":0.0025,"precipProbability":0.05,"precipType":"rain","temperature":1.46,"apparentTemperature":-1.22,"dewPoint":-0.97,"humidity":0.84,"pressure":1005.96,"windSpeed":2.42,"windGust":2.62,"windBearing":273,"cloudCover":0,"uvIndex":1,"ozone":427.89},{"time":1521712800,"summary":"Clear","icon":"clear-day","precipIntensity":0,"precipProbability":0,"temperature":2.11,"apparentTemperature":-0.64,"dewPoint":-0.26,"humidity":0.84,"pressure":1005.45,"windSpeed":2.63,"windGust":4.5,"windBearing":266,"cloudCover":0.08,"uvIndex":1,"ozone":426.45},{"time":1521716400,"summary":"Partly Cloudy","icon":"partly-cloudy-day","precipIntensity":0,"precipProbability":0,"temperature":2.26,"apparentTemperature":-0.97,"dewPoint":0.32,"humidity":0.87,"pressure":1004.79,"windSpeed":3.24,"windGust":7.35,"windBearing":283,"cloudCover":0.31,"uvIndex":1,"ozone":424.23},{"time":1521720000,"summary":"Partly Cloudy","icon":"partly-cloudy-day","precipIntensity":0,"precipProbability":0,"temperature":2.65,"apparentTemperature":-0.93,"dewPoint":0.69,"humidity":0.87,"pressure":1004.32,"windSpeed":3.85,"windGust":9.28,"windBearing":276,"cloudCover":0.44,"uvIndex":1,"ozone":423.09},{"time":1521723600,"summary":"Partly Cloudy","icon":"partly-cloudy-day","precipIntensity":0,"precipProbability":0,"temperature":3.31,"apparentTemperature":-0.05,"dewPoint":0.89,"humidity":0.84,"pressure":1004.15,"windSpeed":3.74,"windGust":9.16,"windBearing":277,"cloudCover":0.32,"uvIndex":1,"ozone":423.95},{"time":1521727200,"summary":"Clear","icon":"clear-day","precipIntensity":0,"precipProbability":0,"temperature":4.12,"apparentTemperature":1.18,"dewPoint":0.93,"humidity":0.8,"pressure":1004.2,"windSpeed":3.38,"windGust":8.13,"windBearing":278,"cloudCover":0.11,"uvIndex":1,"ozone":425.94},{"time":1521730800,"summary":"Clear","icon":"clear-day","precipIntensity":0,"precipProbability":0,"temperature":4.37,"apparentTemperature":1.67,"dewPoint":0.76,"humidity":0.77,"pressure":1004.26,"windSpeed":3.11,"windGust":7.64,"windBearing":281,"cloudCover":0.04,"uvIndex":0,"ozone":428.37},{"time":1521734400,"summary":"Clear","icon":"clear-day","precipIntensity":0,"precipProbability":0,"temperature":3.53,"apparentTemperature":0.84,"dewPoint":0.27,"humidity":0.79,"pressure":1004.36,"windSpeed":2.88,"windGust":8.53,"windBearing":341,"cloudCover":0.22,"uvIndex":0,"ozone":431.47},{"time":1521738000,"summary":"Partly Cloudy","icon":"partly-cloudy-day","precipIntensity":0,"precipProbability":0,"temperature":2,"apparentTemperature":-1.1,"dewPoint":-0.47,"humidity":0.84,"pressure":1004.51,"windSpeed":3,"windGust":9.96,"windBearing":212,"cloudCover":0.53,"uvIndex":0,"ozone":435.08},{"time":1521741600,"summary":"Mostly Cloudy","icon":"partly-cloudy-night","precipIntensity":0.0025,"precipProbability":0.06,"precipAccumulation":0,"precipType":"snow","temperature":0.93,"apparentTemperature":-2.47,"dewPoint":-1.11,"humidity":0.86,"pressure":1004.65,"windSpeed":3.1,"windGust":10.91,"windBearing":270,"cloudCover":0.78,"uvIndex":0,"ozone":437.61},{"time":1521745200,"summary":"Mostly Cloudy","icon":"partly-cloudy-night","precipIntensity":0.0076,"precipProbability":0.06,"precipAccumulation":0,"precipType":"snow","temperature":0.52,"apparentTemperature":-2.78,"dewPoint":-1.49,"humidity":0.86,"pressure":1004.85,"windSpeed":2.89,"windGust":11.04,"windBearing":269,"cloudCover":0.92,"uvIndex":0,"ozone":438.95},{"time":1521748800,"summary":"Overcast","icon":"cloudy","precipIntensity":0.0305,"precipProbability":0.07,"precipAccumulation":0.023,"precipType":"snow","temperature":0.49,"apparentTemperature":-2.59,"dewPoint":-1.84,"humidity":0.84,"pressure":1005.04,"windSpeed":2.66,"windGust":10.68,"windBearing":283,"cloudCover":0.98,"uvIndex":0,"ozone":439.14},{"time":1521752400,"summary":"Overcast","icon":"cloudy","precipIntensity":0.0508,"precipProbability":0.07,"precipAccumulation":0.038,"precipType":"snow","temperature":0.43,"apparentTemperature":-2.55,"dewPoint":-2.21,"humidity":0.82,"pressure":1005.07,"windSpeed":2.54,"windGust":9.65,"windBearing":277,"cloudCover":0.96,"uvIndex":0,"ozone":438.58}]},"offset":1}`
