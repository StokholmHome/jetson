
var departures = {};
var disruptions = {};
var lockStatus = "unlocked";
3
setInterval(function() {
  $.getJSON("http://localhost:8888/departures", function(response) {
    handleTransportation(response);
  });

  $.getJSON("http://localhost:8888/disruptions", function(response) {
    handleDisruptions(response);
  });

  $.getJSON("http://localhost:8888/sesame/status", function(response) {
    handleLockStatus(response);
  });
}, 500);

var weather = {};
$.getJSON("http://localhost:8888/weather", function(response) {
  handleWeather(response);
});

setInterval(function() {
  $.getJSON("http://localhost:8888/weather", function(response) {
    handleWeather(response);
  });
}, 60000);

function handleTransportation(data) {
  departures = {
    bus: []
  }

  data.forEach(function(departure) {
    if (departure.transport_mode == "BUS") {
      departures.bus.push(departure);
    }
  });

  var busDirection1 = '';
  var busDirection2 = '';
  var busDir1Count = 0;
  var busDir2Count = 0;
  departures.bus.forEach(function(departure) {
    if (departure.journey_direction == 1 && busDir1Count < 3) {
      busDirection1 += "<li>" + departure.time_of_departure + "</li>"
      busDir1Count++;
    } else if (departure.journey_direction == 2 && busDir2Count < 3) {
      busDirection2 += "<li>" + departure.time_of_departure + "</li>"
      busDir2Count++;
    }
  });

  $('#bus-direction-1').html(busDirection1);
  $('#bus-direction-2').html(busDirection2);
}

function handleDisruptions(disruptions) {
  $("#train-disruptions").removeClass("disrupted");
  $("#bus-disruptions").removeClass("disrupted");
  $("#metro-disruptions").removeClass("disrupted");

  var disruptionsMessages = "";

  disruptions.forEach(function(disruption) {
    if (!$("#"+disruption.transport_mode+"-disruptions").hasClass("disrupted")) {
      $("#"+disruption.transport_mode+"-disruptions").addClass("disrupted");
    }

    disruptionsMessages += "<p><span>" + disruption.transport_mode + ": " + disruption.header + "</span><br>" + disruption.details + "</p>";
  });

  $("#disruptions").html(disruptionsMessages);
}

function handleLockStatus(lock) {
  lockStatus = "locked";
  if (lock.is_unlocked) {
    lockStatus = "unlocked";
  }

  $("#lock-toggle").removeClass("locked");
  $("#lock-toggle").removeClass("unlocked");
  $("#lock-toggle").addClass(lockStatus);
}

$(document).ready(function() {
  $(".status").click(function() {
    if ($(".disruption-panel").is(':visible')) {
      $(".disruption-panel").hide();
    } else {
      $(".disruption-panel").show();
    }
  });

  $(".disruption-panel").on("click", function(event) {
    $(".disruption-panel").hide();
  });

  setInterval(function() {
    var date = new Date();
    var hours = (`0${date.getHours()}`).slice(-2);
    var minutes = (`0${date.getMinutes()}`).slice(-2);

    $("#hours").html(hours);
    $("#minutes").html(minutes);
  }, 1000);

  document.addEventListener('touchmove', function(event){
    if(event.touches.length >=2) {
      event.stopPropagation();
      event.preventDefault();
    }
  });

  $(".lock li").on('click', function(e) {
    e.preventDefault();
    $(this).addClass("active");
    var that = $(this);

    if ($(this).attr("id") == "lock-delay") {
      setTimeout(function() {
        toggleLock(that, "lock");
      }, 5000);
    } else {
      var action = "lock";
      if (lockStatus === "locked") {
        action = "unlock";
      }
      toggleLock(that, action)
    }
  });
});

function toggleLock(elem, action) {

  if (action == "unlock") {
    elem.removeClass("unlocked");
    elem.addClass("locked");
  } else {
    elem.removeClass("locked");
    elem.addClass("unlocked");
  }

  var failureTimeout = setTimeout(function() {
    elem.removeClass("active");
  }, 60000); // 1 minute

  $.ajax({
    type: "POST",
    url: "http://localhost:8888/sesame/status",
    data: JSON.stringify({status: action}),
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    success: function() {
      clearTimeout(failureTimeout);
      setTimeout(function() {
        elem.css("background-color", "#BB42CD");
        elem.removeClass("active");
        setTimeout(function() {
          elem.css("background-color", "transparent");
        }, 3000);
      }, 3000);
    },
    failure: function(errMsg) {
      clearTimeout(failureTimeout);
      elem.css("background-color", "#00C7C7");
      elem.removeClass("active");
      setTimeout(function() {
        elem.css("background-color", "transparent");
      }, 3000);
    }
  })
}

function handleWeather(data) {

  $('.weather').html('');
  var weatherString = '';
  data.forEach(function(weatherSlot) {
    weatherString += '<ul>\
         <li><img src="icons/weather/'+weatherSlot.icon+'.png"></li>\
         <li class="small">'+weatherSlot.start_hour+'-'+weatherSlot.end_hour+'</li>\
         <li>'+Math.round(weatherSlot.max_temp)+'&deg;</li>\
         <li>'+Math.round(weatherSlot.min_temp)+'&deg;</li>\
       </ul>';
  });

  $('.weather').html(weatherString);
}
