# Jetson - Home Hallway Monitor

[![pipeline status](https://gitlab.com/StokholmHome/jetson/badges/master/pipeline.svg)](https://gitlab.com/StokholmHome/jetson/commits/master)
[![coverage report](https://gitlab.com/StokholmHome/jetson/badges/master/coverage.svg)](https://gitlab.com/StokholmHome/jetson/commits/master)

Jetson is the screen mounted by the door in my home. It will tell you the time, public transportation disruptions, departure times and let you lock or unlock the front door.

![Jetson](readme-files/jetson.jpg "Jetson")

## TODO:

* Control brightness of screen for fade up and down at on and off. See [python lib here](https://github.com/linusg/rpi-backlight/blob/master/rpi_backlight.py).
	* See readme of same repo for [setting permissions](https://github.com/linusg/rpi-backlight#installation).
* Switch to web sockets instead of http requests.
* Create a setup/configuration tool to help generate the `sites` structure in the config file. Specifically the `directions` array is confusing.
* Update readme file with more information.

## Test suite todo
	- [x] Darksky
	- [ ] Transportation
		- [ ] Disruptions
		- [ ] Departures
	- [-] PIR
		- [ ] Init function
		- [x] ScreenOn
		- [x] ScreenOff
	- [ ] Web
	- [ ] Sesame
	- [ ] main.go

## TrafikLab API

TrafikLab is an awesome service available here in Sweden, that exposes API's for a bunch of transportation services. Jetson uses the following API's.

* [SL Realtidsinformation 4 (SL Real Time Information)](https://www.trafiklab.se/api/sl-realtidsinformation-4)
* [SL Störningsinformation 2 (SL Disruption Information)](https://www.trafiklab.se/api/sl-storningsinformation-2)

To get started, go to [TrafikLab](https://www.trafiklab.se) and create an account and then a project. Enable the API's listed here.

You also need to use the [SL Platsuppslag (SL Location Lookup)](https://www.trafiklab.se/api/sl-platsuppslag) API to find the ID's of the stops you care about.

The request looks like this: `http://api.sl.se/api2/typeahead.json?key=<YOUR_API_KEY>&searchstring=<STOP_NAME>&maxresults=10`. It returns JSON, which you should be able to just lift the `SiteId`'s from, and put them in the config.

What level of API key you need depends on how much time the screen spends being on. My Jetson screen is mounted next to the door in my hallway. See the screenshot below for a graph of API calls.

![TrafikLab API Calls Graph](readme-files/api-call-graph.png "TrafikLab API Calls")

The black line is the Real Time API, and the green line is the Disruptions API.

## Hardware

While the hardware is 100% optional, I am running it on the following:

* [Raspberry Pi 3 B](https://www.raspberrypi.org/products/raspberry-pi-3-model-b-plus/)
* [Raspberry Pi 7" touch screen](https://www.raspberrypi.org/products/raspberry-pi-touch-display/) + [official case](https://thepihut.com/products/raspberry-pi-official-7-touchscreen-case)
* 3 ampere wall wart (More would be better, I am seeing the under-voltage icon now and then)
* [PIR motion sensor](https://thepihut.com/products/adafruit-pir-motion-sensor) (Remember dupont cables)
* 16 GB Micro SD card (Hosting Raspbian, with a bunch of yet to be documented software)
